int main(const int argc, const char* argv[])
{
	/*
 	* Return Value : 0 - Job done.
 	*			      1 - Not finished and will be called later.
 	*				  Others - Error code.
 	*/
	extern int start_din(const int start_srv);

	if (start_din(0) == 0)
	{
		return 0;
	}

	// you may add implementation here

	return start_din(1);
}